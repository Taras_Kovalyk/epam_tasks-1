using System;
using System.Text;

namespace Vector
{
    class Vector
    {
        // координати вектора
        private double _x;
        private double _y;
        private double _z;
        // Конструктор за замовчуванням
        public Vector()
        {
            _x = 0;
            _y = 0;
            _z = 0;
        }
        // Конструктор для створення вектора із заданими координатами
        public Vector(double x, double y, double z)
        {
            _x = x;
            _y = y;
            _z = z;
        }
        // Властивості
        public double X
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
            }
        }

        public double Y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
            }

        }
        public double Z
        {
            get
            {
                return _z;
            }
            set
            {
                _z = value;
            }

        }
        /// <summary>
        /// Перегрузка оператора додавання
        /// </summary>
        /// <param name="firstVector"></param>
        /// <param name="secondVector"></param>
        /// <returns>Сума двох векторів</returns>
        public static Vector operator +(Vector firstVector, Vector secondVector)
        {
            return new Vector(firstVector.X + secondVector.X, firstVector.Y + secondVector.Y, firstVector.Z + secondVector.Z);
        }
        /// <summary>
        /// Перегрузка оператора віднімання
        /// </summary>
        /// <param name="firstVector"></param>
        /// <param name="secondVector"></param>
        /// <returns>Різниця двох векторів</returns>
        public static Vector operator -(Vector firstVector, Vector secondVector)
        {
            return new Vector(firstVector.X - secondVector.X, firstVector.Y - secondVector.Y, firstVector.Z - secondVector.Z);
        }
        /// <summary>
        /// Обчислення скалярного добутку двох векторів
        /// </summary>
        /// <param name="firstVector"></param>
        /// <returns>Скалярний добуток двох векторів</returns>
        public double ScalarProduct(Vector firstVector)
        {
            return firstVector.X * _x + firstVector.Y * _y + firstVector.Z * _z;
        }
        /// <summary>
        /// Обчислення векторного добутку двох векторів
        /// </summary>
        /// <param name="firstVector"></param>
        /// <returns>Векторний добуток двох векторів</returns>
        public Vector VectorProduct(Vector firstVector)
        {
            return new Vector(_y * firstVector.Z - _z * firstVector.Y, _z * firstVector.X - _x * firstVector.Z, _x * firstVector.Y - _y * firstVector.X);
        }
        /// <summary>
        /// Обчислення мішаного добутку двох векторів
        /// </summary>
        /// <param name="firstVector"></param>
        /// <returns>Мішаний добуток двох векторів</returns>
        public double TripleProduct(Vector firstVector, Vector secondVector)
        {
            return (_x * firstVector.Y * secondVector.Z) + (_y * firstVector.Z * secondVector.X) + (_z * firstVector.X * secondVector.Y) - (_z * firstVector.Y * secondVector.X) - (_y * firstVector.X * secondVector.Z) - (_x * firstVector.Z * secondVector.Y);
        }
        public double VectorLength()
        {
            return Math.Sqrt((_x * _x) + (_y * _y) + (_z * _z));
        }
        /// <summary>
        /// Перегрузка оператора недорівнює
        /// </summary>
        /// <param name="firstVector"></param>
        /// <param name="secondVector"></param>
        public static bool operator !=(Vector firstVector, Vector secondVector)
        {
            if (firstVector.X != secondVector.X || firstVector.Y != secondVector.Y || firstVector.Z != secondVector.Z)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Перегрузка оператора дорівнює
        /// </summary>
        /// <param name="firstVector"></param>
        /// <param name="secondVector"></param>
        public static bool operator ==(Vector firstVector, Vector secondVector)
        {
         
            return !(firstVector != secondVector);
        }
        // Перегрузка методу Equals для перевірки рівності об'єктів
        public override bool Equals(object obj)
        {
            try
            {
                return (bool)(this == (Vector)obj);
            }
            catch
            {
                // Якщо об'єкт не належить до класу Matrix
                return false;
            }
        }
        // Перегрузка методу GetHashCode для повернення хеш-коду об'єкта
        public override int GetHashCode()
        {
            return (int)(_x * _y * _z);
        }
        /// <summary>
        /// Виведення вектора на екран
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder("", 16);
            sb.Append("(").Append(_x).Append(", ").Append(_y).Append(", ").Append(_z).Append(")");
            return sb.ToString();
        }


        public static void Main(string[] args)
        {
            Vector v1 = new Vector(3, 5, 1);
            Vector v2 = new Vector(2, 4, 8);
            Vector v3 = new Vector(4, 2, 5);

            Console.WriteLine("First vector: {0}", v1);
            Console.WriteLine("Second vector: {0}", v2);

            Console.WriteLine("Sum of the vectors: {0}", v1 + v2);
            Console.WriteLine("Difference of the vectors: {0}", v1 - v2);

            Console.WriteLine("Scalar Product: {0}", v1.ScalarProduct(v2));
            Console.WriteLine("Vector Product: {0}", v1.VectorProduct(v2));
            Console.WriteLine("Triple Product: {0}", v1.TripleProduct(v2, v3));

            Console.WriteLine("Vector length: {0}", v1.VectorLength());
            Console.WriteLine("Angle: {0}", v1.ScalarProduct(v2)/(v1.VectorLength()*v2.VectorLength()));

            if (v1 == v2)
            {
                Console.WriteLine("Equal");
            }
            else
            {
                Console.WriteLine("Not Equal");
            }
        }
    }
}