using System;
using System.Text;

namespace Vector
{
class Vector
    {
        // Одномірний масив
        private int[] _vector;
        
        // Конструктор за замовчуванням
        public Vector()
        {
            this._vector = new int[0];
        }
        
        // Конструктор для створення масиву заданого розміру
        public Vector(int size)
        {
            this._vector = new int[size];
        }
        /// <summary>
        /// Отримання даних з масиву по індексу
        /// </summary>
        /// <param name="index"></param>
        /// <returns>Значення елемента</returns>
        public int this[int index] 
        {
            get
            {
                int res = int.MinValue;
                try
                {
 
                    res = this._vector[index];
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Error: vector doesn't have {0} element!", index);
                }
 
                return res;
            }
            set
            {
                if (index > -1 && index < this._vector.Length)
                    this._vector[index] = value;
            }
        }
        /// <summary>
        /// Кількість елементів
        /// </summary>
        public int Count
        {
            get { return this._vector.Length; }
        }
        /// <summary>
        /// Перегрузка оператора додавання
        /// </summary>
        /// <param name="firstVector"></param>
        /// <param name="secondVector"></param>
        /// <returns>Сума двох векторів</returns>
        public static Vector operator +(Vector firstVector, Vector secondVector)
        {
            Vector sum = new Vector();
            if (firstVector.Count == secondVector.Count)
            {
                sum = new Vector(firstVector.Count);
                for (int i = 0; i < sum.Count; i++)
                {
                    sum[i] = firstVector[i] + secondVector[i];
                }
            }
            return sum;
        }
        /// <summary>
        /// Перегрузка оператора віднімання
        /// </summary>
        /// <param name="firstVector"></param>
        /// <param name="secondVector"></param>
        /// <returns>Різниця двох векторів</returns>
        public static Vector operator -(Vector firstVector, Vector secondVector)
        {
            Vector difference = new Vector();
            if (firstVector.Count == secondVector.Count)
            {
                difference = new Vector(firstVector.Count);
                for (int i = 0; i < difference.Count; i++)
                {
                    difference[i] = firstVector[i] - secondVector[i];
                }
            }
            return difference;
        }
        /// <summary>
        /// Перегрузка оператора множення
        /// </summary>
        /// <param name="firstVector"></param>
        /// <param name="scalar"></param>
        /// <returns>Добуток вектора на скаляр</returns>
        public static Vector operator *(Vector firstVector, int scalar)
        {
            Vector product = new Vector(firstVector.Count);
            for (int i = 0; i < firstVector.Count; i++)
            {
                product[i] = firstVector[i] * scalar;
            }
            return product;
        }
        /// <summary>
        /// Перегрузка оператора недорівнює
        /// </summary>
        /// <param name="firstVector"></param>
        /// <param name="secondVector"></param>
        public static bool operator !=(Vector firstVector, Vector secondVector)
        {
            if (firstVector.Count != secondVector.Count)
                return true;

            for (int i = 0; i < firstVector.Count; i++)
            {
                if (firstVector[i] != secondVector[i])
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Перегрузка оператора недорівнює
        /// </summary>
        /// <param name="firstVector"></param>
        /// <param name="secondVector"></param>
        public static bool operator ==(Vector firstVector, Vector secondVector)
        {
            return !(firstVector != secondVector);
        }
        /// <summary>
        /// Виведення значень елементів масиву на екран
        /// </summary>
        public override string ToString()
        {
            var sb = new StringBuilder("Vector: ", 32);

            for (int i = 0; i < _vector.Length; i++)
            {
                sb.Append(_vector[i]).Append(" ");
            }
            return sb.ToString();
        }
        /// <summary>
        /// Виведення значення конкретного елемента масиву на екран
        /// </summary>
        public void Print(int index)
        {
            if (index > -1 && index < _vector.Length)
                Console.WriteLine("{0} element have value {1}", index, _vector[index]);
        }
    }
 
    class Program
    {
        public static void Main(string[] args)
        {
            Vector arr1 = new Vector(10);
            Vector arr2 = new Vector(10);
            Random rand = new Random();
            for (int i = 0; i < arr1.Count; i++)
            {
                arr1[i] = rand.Next(-10, 10);
            }

            for (int i = 0; i < arr2.Count; i++)
            {
                arr2[i] = rand.Next(-10, 10);
            }

            Console.WriteLine(arr1);
            Console.WriteLine(arr2);
 
            Vector arr3 = arr1 + arr2;
            Console.WriteLine(arr3);
            arr3 = arr1 - arr2;
            Console.WriteLine(arr3);
            arr3 = arr1 * 2;
            Console.WriteLine(arr3);
 
            arr3.Print(2);

            Console.WriteLine(arr3[11]);
            Console.WriteLine(arr3);

            if (arr1 == arr2)
            {
                Console.WriteLine("first and second vectors are equal");
            }
            else 
            {
                Console.WriteLine("first and second vectors are not equal");
            }
        }
    }
}